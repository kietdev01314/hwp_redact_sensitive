import flask
from flask import request, jsonify
from redaction.redactor import redact_names
import json

app = flask.Flask(__name__)
app.config["DEBUG"] = True


@app.route('/', methods=['POST'])
def parse_request():
        datastr = request.json
        datadumps = json.dumps(datastr)
        after_redactData = redact_names(datadumps)
        resultJsonify = after_redactData[0]
        return resultJsonify
app.run(host='0.0.0.0', port=5000)
