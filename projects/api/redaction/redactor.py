import nltk
nltk.download('punkt')
nltk.download('averaged_perceptron_tagger')
nltk.download('maxent_ne_chunker')
nltk.download('words')
#from nltk.tokenize import RegexpTokenizer

stats_list = []

def redact_names(files_data):
    names_list = []
    NamesRedacted_List = []

    temp_file = files_data
    words = nltk.word_tokenize(temp_file)
    tagged = nltk.pos_tag(words)
    namedEnt = nltk.ne_chunk(tagged)
    for chunk in namedEnt:
        if type(chunk) == nltk.tree.Tree:
            if chunk.label() == 'PERSON':
                names_list.append(' '.join([c[0] for c in chunk]))
    for names in names_list:
        print(names)
        temp_file = temp_file.replace( names, '\u2588')
    NamesRedacted_List.append(temp_file)
    redacted_stats(string,len(names_list))
    names_list.clear()
    return NamesRedacted_List

# concept = "good"

def redacted_stats(redacted_type= 'none', count=0):

    if redacted_type =='redacted_names':
        temp = "The count of " + redacted_type + " : " + str (count)
        stats_list.append(temp)
        # print(stats_list)
    elif redacted_type == 'redacted_dates':
        temp = "The count of " + redacted_type + " : " + str(count)
        stats_list.append(temp)
    elif redacted_type == 'redacted_gender':
        temp = "The count of " + redacted_type + " : " + str(count)
        stats_list.append(temp)
    elif redacted_type == 'redacted_concept':
        temp = "The count of " + redacted_type + " : " + str(count)
        stats_list.append(temp)

    # Update_Redacted_stats(stats_list)
    # print(len(stats_list))
    return stats_list


