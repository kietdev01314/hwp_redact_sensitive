# HWP_redact_sensitive

### **Gitlab URL**
 - URL: https://gitlab.com/kietdev01314/hwp_redact_sensitive

### **Jenkins website**
 - I hosted jenkins container in a VM and get webhook from gitlab. 
 - Web IP: http://69.55.59.163:8080/
 - Username: reviewer
 - Password: **attached in mail**

### **Redact application**
 - I also hosted redact application for you to easy to test and check 
 - URL: http://69.55.59.163:8001
 - Request: POST
 - Body: application/json
  
### **Package the service as a container & deploy to a k8s cluster**
 I create Dockerfile for containerizing. You can check in repository
 ```shell
 $ cd projects/api
 $ docker build -f Dockerfile -t hwp/redact . 
 ```  
 In my local, I created file hwp-deployment.yaml and used kubectl command to deploy my application
 ```shell
 $ kubectl apply -f hwp-deployment.yaml
 ```  
 ```yaml
  apiVersion: apps/v1
  kind: Deployment
  metadata:
    name: hwp-app-deployment
    labels:
      app: hwp-app
  spec:
    replicas: 3
    selector:
      matchLabels:
        app: hwp-app
    template:
      metadata:
        labels:
          app: hwp-app
      spec:
        containers:
        - name: hwp-app
          imagePullPolicy: Never
          image: hwp/redact
          ports:
          - containerPort: 5000
  ---
  apiVersion: v1
  kind: Service
  metadata:
    name: hwp-app-service
  spec:
    type: LoadBalancer
    ports:
    - port: 8001
      targetPort: 5000
    selector:
      app: hwp-app
 ```

### **Implement CI in Jenkins**
 - Install following plugins: Gitlab , Git parameters, Managed script
 - I use Jenkins to get a webhook by Gitlab when any new branch is pushed. 
 - I create a shell script to build image and run simple docker container for live testing.

  ***NOTE***:
 - For improving current CI. I would like to use Ansible and Helm to deploy service to Kubernetes.
 - The pipeline will have these steps:
   - Step 1: Pull/Clone new codes from GitLab
   - Step 2: Run python test 
   - Step 3: Docker build image and push to docker registry( Docker hub or Azure Container Registry)
   - Step 4: Jenkins execute ansible playbook to deploy to K8s, ansible book defines flow of helm command to install and upgrade pods.
 - For extra small step. I would strictly at permission of pipelines, adding morethings like: SonarQube to review the codes, and Condition when pipelines will deploy an application. Furthermore, I will check branch name or tag to match a rule such as only deploy when branch name equal: origin/release-{ENVIRONMENT}-{version}  
 
### **Handle zero-downtime upgrades of the service**  
About this solution, I will use Rolling Update to cover zero-downtime. I will add Readliness probe to check avalability of application. Because a request to pod is affected by the status of application. 
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: hwp-app-deployment
spec:
  replicas: 3
  selector:
    matchLabels:
      app: hwp-app 
  template:
    metadata:
      labels:
        app: hwp-app
    spec:
      containers:
      - image: hwp/redact:v1
        readinessProbe:
          httpGet:
             path: /test
             port: 5000
          initialDelaySeconds: 5
          periodSeconds: 5
          successThreshold: 1
        imagePullPolicy: Never
        name: hwp-app
        ports:
        - containerPort: 5000
---
apiVersion: v1
kind: Service
metadata:
  name: hwp-app-service
spec:
  type: LoadBalancer
  ports:
  - port: 8001
    targetPort: 5000
  selector:
    app: hwp-app
```
Moreover, I intercede to a lifecycle hook of k8s called preStop to solving time of update routing in k8s
```yaml
    containers:
      - image: hwp/redact:v1
        readinessProbe:
          httpGet:
            path: /test
            port: 5000
          initialDelaySeconds: 5
          periodSeconds: 5
          successThreshold: 1
        lifecycle:
          preStop:
            exec:
              command: ["/bin/bash", "-c", "sleep 15"]
```
It will make application sleep about 15 second before there are a signaling off to pod
